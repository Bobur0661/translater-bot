package uz.pdp.bot;

public interface LangInterface {

    String engToRu = "en-ru";
    String ruToEng = "ru-en";
    String turkToEng = "tr-en";
    String engToTurk = "en-tr";
    String ruToTurk = "ru-tr";
    String turkToRu = "tr-ru";

}
