package uz.pdp.bot;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import lombok.SneakyThrows;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardButton;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.KeyboardRow;
import uz.pdp.db.DB;
import uz.pdp.model.*;
import org.telegram.telegrambots.meta.api.objects.Update;
import uz.pdp.responseFromYandexApi.Response;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

public class BotService {


    public static SendMessage start(Update update) {
        String chatId = getChatId(update);
        TgUser tgUser = getOrCreateTgUser(chatId);
        SendMessage sendMessage = new SendMessage();
        tgUser.setState(BotState.START);
        saveUserChanges(tgUser);
        sendMessage.setText("Quyidagi tugmalardan birini tanlang:");
        sendMessage.setChatId(chatId);
        ReplyKeyboardMarkup markup = generateReplyKeyBoardButton(tgUser);
        sendMessage.setReplyMarkup(markup);
        return sendMessage;
    }

    private static ReplyKeyboardMarkup generateReplyKeyBoardButton(TgUser tgUser) {
        ReplyKeyboardMarkup markup = new ReplyKeyboardMarkup();
        List<KeyboardRow> rowList = new ArrayList<>();
        KeyboardRow row1 = new KeyboardRow();
        KeyboardButton row1Button1 = new KeyboardButton();
        if (tgUser.getState().equals(BotState.START)) {
            // 1 - qator ba unda 2 ta button bor
            row1Button1.setText(LangInterface.engToRu);
            KeyboardButton row1Button2 = new KeyboardButton();
            row1Button2.setText(LangInterface.ruToEng);
            row1.add(row1Button1);
            row1.add(row1Button2);

            // 2 - qator ikkita button bor
            KeyboardRow row2 = new KeyboardRow();
            KeyboardButton row2Button1 = new KeyboardButton();
            row2Button1.setText(LangInterface.turkToEng);
            KeyboardButton row2Button2 = new KeyboardButton();
            row2Button2.setText(LangInterface.ruToTurk);
            row2.add(row2Button1);
            row2.add(row2Button2);

            // 3 - qator ikkita button bor
            KeyboardRow row3 = new KeyboardRow();
            KeyboardButton row3Button1 = new KeyboardButton();
            row3Button1.setText(LangInterface.engToTurk);
            KeyboardButton row3Button2 = new KeyboardButton();
            row3Button2.setText(LangInterface.turkToRu);
            row3.add(row3Button1);
            row3.add(row2Button2);
            rowList.add(row1);
            rowList.add(row2);
            rowList.add(row3);
        }
        markup.setKeyboard(rowList);
        markup.setSelective(true);
        markup.setResizeKeyboard(true);
        markup.setOneTimeKeyboard(true);
        return markup;
    }


    public static void saveUserChanges(TgUser changedUser) {
        for (TgUser user : DB.userList) {
            if (user.getChatId().equals(changedUser.getChatId())) {
                user = changedUser;
            }
        }
    }

    public static TgUser getOrCreateTgUser(String chatId) {
        for (TgUser user : DB.userList) {
            if (user.getChatId().equals(chatId)) {
                return user;
            }
        }
        TgUser user = new TgUser(chatId, BotState.START);
        DB.userList.add(user);
        return user;
    }

    public static String getChatId(Update update) {
        if (update.hasMessage()) {
            return update.getMessage().getChatId().toString();
        } else if (update.hasCallbackQuery()) {
            return update.getCallbackQuery().getMessage().getChatId().toString();
        }
        return "";
    }

    public static SendMessage enterTheWord(TgUser user) {
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(user.getChatId());
        sendMessage.setText("So'z kiriting: ");
        return sendMessage;
    }

    @SneakyThrows
    public static SendMessage returnTransOfEnteredWord(TgUser user, String userText) {
        String chatId = user.getChatId();
        String chosenLang = user.getSelectedLang();
        SendMessage sendMessage = new SendMessage();
        sendMessage.setChatId(chatId);

        URL url = new URL("https://dictionary.yandex.net/api/v1/dicservice.json/lookup?" +
                "key=dict.1.1.20210412T160607Z.113f8372b4e893f8.49f2f5644fc4aee6a6c2056dc57bc9b5a6060991&lang=" + chosenLang + "&text=" + userText);
        URLConnection urlConnection = url.openConnection();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        List<Response> responseList = new ArrayList<>();
        Response response = gson.fromJson(bufferedReader, Response.class);
        responseList.add(response);
        sendMessage.setText(responseList.get(0).getDef().get(0).getTr().get(0).getText() + "\n Til tanlang!");
        user.setState(BotState.START);
        saveUserChanges(user);
        return sendMessage;
    }


}
