package uz.pdp.bot;

public interface BotState {


    String START = "START";
    String CHOOSE_LANG = "CHOOSE_LANG";
    String ENTER_WORD = "ENTER_WORD";
    String GET_TRANS_WORD = "GET_TRANS_WORD";

}
