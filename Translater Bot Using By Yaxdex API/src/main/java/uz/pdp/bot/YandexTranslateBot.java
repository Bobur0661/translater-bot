package uz.pdp.bot;

import lombok.SneakyThrows;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.DeleteMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.ReplyKeyboardRemove;
import uz.pdp.model.TgUser;


public class YandexTranslateBot extends TelegramLongPollingBot {


    @Override
    public String getBotUsername() {
        return "yandex_tarjimon_Bot_bot";
    }

    @Override
    public String getBotToken() {
        return "5094370720:AAG7tLj-ynigqLxGFjXCHr6a_BzqB_b11CE";
    }

    @SneakyThrows
    @Override
    public void onUpdateReceived(Update update) {

        if (update.hasMessage()) {
            TgUser user = BotService.getOrCreateTgUser(BotService.getChatId(update));
            String text = update.getMessage().getText();
            if (text.equals("/start")) {
                execute(BotService.start(update));
            } else if (user.getState().equals(BotState.START)) {
                user.setState(BotState.CHOOSE_LANG);
                BotService.saveUserChanges(user);
                if (text.equals(LangInterface.engToRu)) {
                    user.setSelectedLang(LangInterface.engToRu);
                    user.setState(BotState.ENTER_WORD);
                    BotService.saveUserChanges(user);
                    execute(BotService.enterTheWord(user));
                } else if (text.equals(LangInterface.ruToEng)) {
                    user.setSelectedLang(LangInterface.ruToEng);
                    user.setState(BotState.ENTER_WORD);
                    BotService.saveUserChanges(user);
                    execute(BotService.enterTheWord(user));
                } else if (text.equals(LangInterface.turkToEng)) {
                    user.setSelectedLang(LangInterface.turkToEng);
                    user.setState(BotState.ENTER_WORD);
                    BotService.saveUserChanges(user);
                    execute(BotService.enterTheWord(user));
                } else if (text.equals(LangInterface.ruToTurk)) {
                    user.setSelectedLang(LangInterface.ruToTurk);
                    user.setState(BotState.ENTER_WORD);
                    BotService.saveUserChanges(user);
                    execute(BotService.enterTheWord(user));
                } else if (text.equals(LangInterface.engToTurk)) {
                    user.setSelectedLang(LangInterface.engToTurk);
                    user.setState(BotState.ENTER_WORD);
                    BotService.saveUserChanges(user);
                    execute(BotService.enterTheWord(user));
                } else if (text.equals(LangInterface.turkToRu)) {
                    user.setSelectedLang(LangInterface.turkToRu);
                    user.setState(BotState.ENTER_WORD);
                    BotService.saveUserChanges(user);
                    execute(BotService.enterTheWord(user));
                }
            } else if (user.getState().equals(BotState.ENTER_WORD)) {
                String userEnterWord = update.getMessage().getText();
                if (user.getSelectedLang().equals(LangInterface.engToRu)) {
                    user.setState(BotState.GET_TRANS_WORD);
                    BotService.saveUserChanges(user);
                    execute(BotService.returnTransOfEnteredWord(user, userEnterWord));
                } else if (user.getSelectedLang().equals(LangInterface.ruToEng)) {
                    user.setState(BotState.GET_TRANS_WORD);
                    BotService.saveUserChanges(user);
                    execute(BotService.returnTransOfEnteredWord(user, userEnterWord));
                } else if (user.getSelectedLang().equals(LangInterface.engToTurk)) {
                    user.setState(BotState.GET_TRANS_WORD);
                    BotService.saveUserChanges(user);
                    execute(BotService.returnTransOfEnteredWord(user, userEnterWord));
                } else if (user.getSelectedLang().equals(LangInterface.turkToEng)) {
                    user.setState(BotState.GET_TRANS_WORD);
                    BotService.saveUserChanges(user);
                    execute(BotService.returnTransOfEnteredWord(user, userEnterWord));
                } else if (user.getSelectedLang().equals(LangInterface.ruToTurk)) {
                    user.setState(BotState.GET_TRANS_WORD);
                    BotService.saveUserChanges(user);
                    execute(BotService.returnTransOfEnteredWord(user, userEnterWord));
                } else if (user.getSelectedLang().equals(LangInterface.turkToRu)) {
                    user.setState(BotState.GET_TRANS_WORD);
                    BotService.saveUserChanges(user);
                    execute(BotService.returnTransOfEnteredWord(user, userEnterWord));
                }
//                user.setState(BotState.CHOOSE_LANG);
            }
        }
    }


    @SneakyThrows
    public void deleteMessage(TgUser user) {
        SendMessage sendMessageRemove = new SendMessage();
        sendMessageRemove.setChatId(user.getChatId());
        sendMessageRemove.setText(".");
        sendMessageRemove.setReplyMarkup(new ReplyKeyboardRemove(true));
        Message message = execute(sendMessageRemove);
        DeleteMessage deleteMessage = new DeleteMessage(user.getChatId(), message.getMessageId());
        execute(deleteMessage);
    }
}
