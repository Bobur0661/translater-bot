package uz.pdp.model;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TgUser {

    private String chatId;
    private String state;
    private String selectedLang;

    public TgUser(String chatId, String state) {
        this.chatId = chatId;
        this.state = state;
    }
}
